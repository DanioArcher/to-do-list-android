# ToDo List

First app written in Android Studio using Java. App features:
- add, edit, delete and complete tasks
- set tasks due date
- set reminder with notification
- set task priority and repetitivity
- sort tasks alphabetically, by priority, by due date and by create date
- dark mode
- english and polish language
- swipe left or right to complete or delete the task

<img align="left" width="400" height="823" src="Screenshots/screenshot_1.png">
<img align="right" width="400" height="823" src="Screenshots/screenshot_2.png">

<img align="left" width="400" height="823" src="Screenshots/screenshot_3.png">
<img align="right" width="400" height="823" src="Screenshots/screenshot_4.png">

<img align="left" width="400" height="823" src="Screenshots/screenshot_5.png">
<img align="right" width="400" height="823" src="Screenshots/screenshot_6.png">
