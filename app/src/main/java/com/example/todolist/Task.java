package com.example.todolist;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;

class Task implements Parcelable //Parcelable is used for passing objects through intents
{
    public enum Priority
    {
        LOW(0),
        MEDIUM(1),
        HIGH(2);

        private int number;

        private Priority(int number)
        {
            this.number = number;
        }

        public int getNumber()
        {
            return number;
        }
    }

    public enum Repeat
    {
        NONE(0),
        DAILY(1),
        WEEKLY(2),
        MONTHLY(3),
        YEARLY(4);

        private int number;

        private Repeat(int number)
        {
            this.number = number;
        }

        public int getNumber()
        {
            return  number;
        }
    }

    public String title;
    public String description;
    public Priority priority;
    public Repeat repeat;
    public boolean isCompleted;
    public final Calendar createTime;
    public Calendar dueDate;
    public Calendar reminderTime;

    private final int id;

    public PendingIntent GetPendingIntent(Context context)
    {
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(AlarmReceiver.TASK_ID, id);
        return PendingIntent.getBroadcast(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public Task(String title, String description, Priority priority, Repeat repeat, Calendar createTime, Calendar dueDate, Calendar reminderTime, boolean isCompleted)
    {
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.repeat = repeat;
        this.createTime = createTime;
        this.dueDate = dueDate;
        this.reminderTime = reminderTime;
        this.isCompleted = isCompleted;
        this.id = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE); //it will never repeats values
    }

    public Task(String title, String description, Priority priority, Repeat repeat, Calendar createTime, Calendar dueDate, Calendar reminderTime, boolean isCompleted, int id)
    {
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.repeat = repeat;
        this.createTime = createTime;
        this.dueDate = dueDate;
        this.reminderTime = reminderTime;
        this.isCompleted = isCompleted;
        this.id = id;
    }

    public Task(Task another) //in Java we must define copying constructor
    {
        this.title = another.title;
        this.description = another.description;
        this.priority = another.priority;
        this.repeat = another.repeat;
        this.isCompleted = another.isCompleted;
        //must clone these Calendar objects, because if not, they will be original variable of 'another' object
        this.createTime = (Calendar) another.createTime.clone();

        if(another.dueDate != null)
            this.dueDate = (Calendar) another.dueDate.clone();
        else
            this.dueDate = null;

        if(another.reminderTime != null)
            this.reminderTime = (Calendar) another.reminderTime.clone();
        else
            this.reminderTime = null;

        this.id = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE); //another.id;
    }

    public Task(Parcel source)
    {
        //reading from Parcel is done in FIFO
        title = source.readString();
        description = source.readString();

        int temp = source.readInt();
        for(Priority myPriority : Priority.values())
            if(myPriority.getNumber() == temp)
            {
                priority = myPriority;
                break;
            }

        temp = source.readInt();
        for(Repeat myRepeat : Repeat.values())
            if(myRepeat.getNumber() == temp)
            {
                repeat = myRepeat;
                break;
            }

        isCompleted = source.readInt() == 1;
        createTime = (Calendar) source.readSerializable();
        dueDate = (Calendar) source.readSerializable();
        reminderTime = (Calendar) source.readSerializable();
        id = source.readInt();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeInt(priority.getNumber());
        dest.writeInt(repeat.getNumber());
        dest.writeInt(isCompleted ? 1 : 0);
        dest.writeSerializable(createTime);
        dest.writeSerializable(dueDate);
        dest.writeSerializable(reminderTime);
        dest.writeInt(id);
    }

    public static final Parcelable.Creator<Task> CREATOR = new Parcelable.Creator<Task>()
    {
        @Override
        public Task createFromParcel(Parcel source)
        {
            return new Task(source);
        }

        @Override
        public Task[] newArray(int size)
        {
            return new Task[size];
        }
    };

    public int GetID()
    {
        return id;
    }
}
