package com.example.todolist;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder>
{
    class TaskViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener
    {
        public View cardView;
        public TextView taskTitle;
        public TextView taskDueDate;
        public TextView taskDescription;
        //public RadioButton taskCompleted;
        public ImageView taskRepeatIcon;
        public ImageView taskAlarmIcon;
        public int itemPosition;

        public TaskViewHolder(View itemView, TaskAdapter adapter)
        {
            super(itemView);

            cardView = itemView.findViewById(R.id.task_item_cardView);

            cardView.setOnClickListener(this);
            cardView.setOnCreateContextMenuListener(this); //floating menu when long click on this item

            taskTitle = itemView.findViewById(R.id.task_title);
            taskDueDate = itemView.findViewById(R.id.task_due_date);
            taskDescription = itemView.findViewById(R.id.task_description);
            taskRepeatIcon = itemView.findViewById(R.id.task_repeat_icon);
            taskAlarmIcon = itemView.findViewById(R.id.task_alarm_icon);

            /*
            taskCompleted = itemView.findViewById(R.id.task_completed);
            taskCompleted.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Container.Tasks.CompleteTask(getItem(itemPosition).GetID(), taskCompleted.isChecked());
                    mainActivity.UpdateTasksView();
                }
            });
            */
        }

        @Override
        public void onClick(View v)
        {
            //Use for Shared Elements
            //Pair<View, String> titlePair = Pair.create(taskTitle, taskTitle.getTransitionName());
            //Pair<View, String> dueDatePair = Pair.create(taskDueDate, taskDueDate.getTransitionName());
            //Pair<View, String> descriptionPair = Pair.create(taskDescription, taskDescription.getTransitionName());
            //mainActivity.DisplayTask(getItem(itemPosition), titlePair, dueDatePair, descriptionPair);

            mainActivity.DisplayTask(getItem(itemPosition));
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) //function to execute when on long click
        {
            MenuInflater inflater = mainActivity.getMenuInflater();
            inflater.inflate(R.menu.task_item_floating_menu, menu);
        }
    }

    private LinkedList<Task> tasksList;
    private LayoutInflater inflater;
    private MainActivity mainActivity;

    /*On long click means opening a floating menu. When an item is being long pressed then set
     *his position in list to this variable. Thanks to that we can tell which task to delete
     *in 'onContextItemSelected' function in MainActivity
     */
    private int onLongClickItemPosition;

    public TaskAdapter(Context context, LinkedList<Task> tasksList, MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
        inflater = LayoutInflater.from(context);
        this.tasksList = tasksList;
    }

    //Method used for undo action when deleting an item with swipe left
    public void restoreItem(Task task, int position)
    {
        tasksList.add(position, task);
        notifyItemInserted(position);

        if(task.reminderTime != null)
            AppUtils.SetAlarm(mainActivity, task);
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = inflater.inflate(R.layout.task_item, parent, false);
        return new TaskViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position)
    {
        Task task = getItem(position);

        //Use for Shared Elements
        //holder.taskTitle.setTransitionName("titleTransition" + position);
        //holder.taskDueDate.setTransitionName("dueDateTransition" + position);
        //holder.taskDescription.setTransitionName("descriptionTransition" + position);

        holder.taskTitle.setText(task.title);
        holder.taskTitle.setTextColor(AppUtils.GetTitleColor(mainActivity, task.priority));

        if(!task.description.equals(""))
        {
            holder.taskDescription.setText(task.description);
            holder.taskDescription.setVisibility(View.VISIBLE);
        }
        else
            holder.taskDescription.setVisibility(View.GONE);

        if(task.dueDate != null)
        {
            holder.taskDueDate.setText(AppUtils.GetDateAsString(mainActivity, task.dueDate, true));
            holder.taskDueDate.setVisibility(View.VISIBLE);
        }
        else
            holder.taskDueDate.setVisibility(View.GONE);

        if(task.repeat.getNumber() != Task.Repeat.NONE.getNumber())
            holder.taskRepeatIcon.setVisibility(View.VISIBLE);
        else
            holder.taskRepeatIcon.setVisibility(View.GONE);

        if(task.reminderTime != null)
            holder.taskAlarmIcon.setVisibility(View.VISIBLE);
        else
            holder.taskAlarmIcon.setVisibility(View.GONE);

        //holder.taskCompleted.setChecked(task.isCompleted);
        holder.itemPosition = position;

        //when on long click then assign this item position in list to 'onLongClickPosition' variable
        holder.cardView.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                onLongClickItemPosition = holder.itemPosition;
                return false;
            }
        });


        if(task.isCompleted)
        {
            //Add flag
            holder.taskTitle.setPaintFlags(holder.taskTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.taskDescription.setPaintFlags(holder.taskDescription.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.taskDueDate.setPaintFlags(holder.taskDueDate.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else
        {
            //Delete flag
            holder.taskTitle.setPaintFlags(holder.taskTitle.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            holder.taskDescription.setPaintFlags(holder.taskDescription.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            holder.taskDueDate.setPaintFlags(holder.taskDueDate.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
    }

    @Override
    public int getItemCount()
    {
        return tasksList.size();
    }

    public Task getItem(int position)
    {
        return tasksList.get(position);
    }

    public int getPosition() { return onLongClickItemPosition; }
}
