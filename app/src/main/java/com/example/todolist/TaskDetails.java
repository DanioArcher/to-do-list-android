package com.example.todolist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.material.internal.VisibilityAwareImageButton;

import org.w3c.dom.Text;

public class TaskDetails extends AppCompatActivity
{
    public final static String TASK_DETAILS_INTENT = "com.example.android.todolist.extra.TASK_DETAILS_INTENT"; //bundle where is Task object, given for this activity

    private Task myTask;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);

        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.task_details_toolbar_title));
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Container.Tasks.Initialize(this);

        TextView titleDetails = findViewById(R.id.title_details);
        TextView descriptionDetails = findViewById(R.id.description_details);
        TextView priorityDetails = findViewById(R.id.priority_details);
        TextView repeatDetails = findViewById(R.id.repeat_details);
        TextView dueDetails = findViewById(R.id.due_details);
        TextView reminderDateDetails = findViewById(R.id.reminder_date_details);
        TextView reminderTimeDetails = findViewById(R.id.reminder_time_details);
        CheckBox completedDetails = findViewById(R.id.completed_details);

        TextView descriptionDetailsLabel = findViewById(R.id.description_label_details);
        TextView dueDetailsLabel = findViewById(R.id.due_label_details);
        TextView reminderDetailsLabel = findViewById(R.id.reminder_label_details);


        Intent intent = getIntent();
        myTask = intent.getParcelableExtra(MainActivity.TASK_INTENT);

        //Displaying details of given task
        titleDetails.setText(myTask.title);

        if(myTask.description.isEmpty())
            SetGroupVisibility(descriptionDetails, descriptionDetailsLabel, View.GONE);
        else
        {
            SetGroupVisibility(descriptionDetails, descriptionDetailsLabel, View.VISIBLE);
            descriptionDetails.setText(myTask.description);
        }

        //priorityDetails.setText(myTask.priority.name());
        //repeatDetails.setText(myTask.repeat.name());
        priorityDetails.setText(AppUtils.GetPriorityName(this, myTask.priority.name(), false));
        repeatDetails.setText(AppUtils.GetRepeatName(this, myTask.repeat.name(), false));


        if(myTask.dueDate == null)
            SetGroupVisibility(dueDetails, dueDetailsLabel, View.GONE);
        else
        {
            SetGroupVisibility(dueDetails, dueDetailsLabel, View.VISIBLE);
            dueDetails.setText(AppUtils.GetDateAsString(this, myTask.dueDate, false));
        }

        if(myTask.reminderTime == null)
        {
            SetGroupVisibility(reminderDateDetails, reminderDetailsLabel, View.GONE);
            SetGroupVisibility(reminderTimeDetails, reminderDetailsLabel, View.GONE);
        }
        else
        {
            SetGroupVisibility(reminderDateDetails, reminderDetailsLabel, View.VISIBLE);
            SetGroupVisibility(reminderTimeDetails, reminderDetailsLabel, View.VISIBLE);
            reminderDateDetails.setText(AppUtils.GetDateAsString(this, myTask.reminderTime, false));
            reminderTimeDetails.setText(AppUtils.GetTimeAsString(this, myTask.reminderTime));
        }

        completedDetails.setChecked(myTask.isCompleted);
    }

    private void SetGroupVisibility(TextView element, TextView label, final int visibility)
    {
        element.setVisibility(visibility);
        label.setVisibility(visibility);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) //creating overflow menu
    {
        getMenuInflater().inflate(R.menu.task_details_menu, menu);
        return true;
    }

    public void MarkTaskAsCompleted(View view)
    {
        Container.Tasks.CompleteTask(myTask.GetID()); //Container.Tasks.CompleteTask(myTask.GetID(), ((CheckBox) view).isChecked());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.details_delete:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.delete_dialog_alert);
                builder.setPositiveButton(R.string.delete_dialog_positive_button, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        Container.Tasks.DeleteTask(myTask.GetID());
                        finish();
                    }
                });
                builder.setNegativeButton(R.string.delete_dialog_negative_button, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                    }
                });

                builder.create().show();
                break;
            case R.id.details_edit:
                Intent intent = new Intent(this, TaskAdd.class);
                intent.putExtra(TASK_DETAILS_INTENT, myTask);
                intent.putExtra(TaskAdd.INTENT_ID, TaskAdd.INTENT_FROM_TASK_DETAILS); //inform, that we will be editing a task
                startActivity(intent);
                finish();
                break;
        }

        return super.onOptionsItemSelected(item); //without this, up button does not work
    }

    @Override
    protected void onStop()
    {
        Container.Tasks.SaveData(this);
        super.onStop();
    }
}